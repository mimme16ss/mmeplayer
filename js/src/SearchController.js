var MmePlayer = MmePlayer || {};
MmePlayer.SearchController = function(options) {
  "use strict";
  /* global MMEUtils */
  
  var that = new MMEUtils.EventPublisher,
    searchInput = options.target;

  function onKeyUp(event) {
    var currentValue = searchInput.value;    
    if (event.code === "Enter" && currentValue.length > 0) {
      that.notifyAll("searchQueryEntered", currentValue);
    }
  }

  function clear() {
    searchInput.value = "";
  }

  searchInput.placeholder = options.placeholderText;
  searchInput.addEventListener("keyup", onKeyUp);

  that.clear = clear;
  return that;
};
