var MmePlayer = (function() {
  "use strict";
  /* eslint-env browser */
  var that = {},
    soundcloudBridge,
    soundcloudPlayer,
    searchController,
    resultlist,
    playlist;

  function onSearchQueryEntered(event) {
    soundcloudBridge.searchSongs(event.data);
    searchController.clear();
  }

  function onResultlistTrackSelected(event) {
    soundcloudPlayer.addTrackToPlaylist(event.data);
  }

  function onPlaylistTrackSelected(event) {
    soundcloudPlayer.selectTrack(event.data);
    soundcloudPlayer.play();
  }

  function onPlaylistTrackPaused() {
    soundcloudPlayer.pause();
  }

  function onPlaylistTrackContinued() {
    soundcloudPlayer.continue();
  }

  function onTrackAddedToPlayer(event) {
    playlist.addTrack(event.data);
  }

  function onTrackStartedByPlayer(event) {
    playlist.setActiveTrack(event.data);
  }

  function onPlayerPlaylistCleared() {
    playlist.clear();
  }

  function onTracksFound(event) {
    resultlist.clear();
    resultlist.renderSearchResults(event.data);
  }

  function init() {
    initUserInterface();
    initSoundCloud();
  }

  function initUserInterface() {
    searchController = new MmePlayer.SearchController({
      target: document.querySelector(".searchbar input"),
      placeholderText: "Insert query and press enter",
    });
    searchController.addEventListener("searchQueryEntered", onSearchQueryEntered);
    resultlist = new MmePlayer.ResultList({
      target: document.querySelector(".resultlist"),
      entryTemplate: document.querySelector("#resultlist-entry").innerHTML,
    });
    resultlist.addEventListener("trackSelected", onResultlistTrackSelected);
    playlist = new MmePlayer.PlayList({
      target: document.querySelector(".tracklist"),
      entryTemplate: document.querySelector("#playlist-entry").innerHTML,
    });
    playlist.addEventListener("trackSelected", onPlaylistTrackSelected);
    playlist.addEventListener("trackPaused", onPlaylistTrackPaused);
    playlist.addEventListener("trackContinued", onPlaylistTrackContinued);
  }

  function initSoundCloud() {
    soundcloudBridge = new MmePlayer.SoundCloudBridge();
    soundcloudBridge.addEventListener("tracksFound", onTracksFound);
    soundcloudBridge.init({
      ID: "3ed7e2e0325513e3339802ca6a8a7d8c",
    });
    soundcloudPlayer = new MmePlayer.SoundCloudPlayer(soundcloudBridge);
    soundcloudPlayer.addEventListener("trackAdded", onTrackAddedToPlayer);
    soundcloudPlayer.addEventListener("trackStarted", onTrackStartedByPlayer);
    soundcloudPlayer.addEventListener("playlistCleared", onPlayerPlaylistCleared);
  }

  that.init = init;
  return that;
}());
