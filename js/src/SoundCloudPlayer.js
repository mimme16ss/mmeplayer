var MmePlayer = MmePlayer || {};
MmePlayer.SoundCloudPlayer = function(sc) {
  "use strict";
  /* global MMEUtils */

  var that = new MMEUtils.EventPublisher(),
    playlist = [],
    TRACK_START_POSITION = 0,
    TIME_BEFORE_SKIPPING_BAD_TRACK = 500,
    currentTrack,
    currentPlayer,
    currentPlayerHasStartedPlaying,
    knownPlayersIds = [];

  function onPlayerError() {
    selectNextTrack();
  }

  function onPlayerStarted() {
    currentPlayerHasStartedPlaying = true;
  }

  function checkIfPlayerHasStarted() {
    if (!currentPlayerHasStartedPlaying) {
      onPlayerError();
    }
  }

  function isPlayerRecycled(player) {
    var i;
    for (i = 0; i < knownPlayersIds.length; i++) {
      if (player.getId() === knownPlayersIds[i]) {
        return true;
      }
    }
    knownPlayersIds.push(player.getId());
    return false;
  }

  function onStreamReady(player) {
    currentPlayer = player;
    currentPlayerHasStartedPlaying = false;
    if (!isPlayerRecycled(currentPlayer)) {
      currentPlayer.on("play-start", onPlayerStarted);
      currentPlayer.on("play-resume", onPlayerStarted);
      currentPlayer.on("finish", selectNextTrack);
      currentPlayer.on("audio_error", onPlayerError);
      currentPlayer.on("geo_blocked", onPlayerError);
      currentPlayer.on("no_streams", onPlayerError);
      currentPlayer.on("no_protocol", onPlayerError);
      currentPlayer.on("no_connection", onPlayerError);
    } else {
      currentPlayer.seek(TRACK_START_POSITION);
    }
    currentPlayer.play();
    setTimeout(checkIfPlayerHasStarted, TIME_BEFORE_SKIPPING_BAD_TRACK);
    that.notifyAll("trackStarted", currentTrack);
  }

  function getTrackPositionFromPlaylist(track) {
    var i;
    for (i = 0; i < playlist.length; i++) {
      if (playlist[i].id === track.id) {
        return i;
      }
    }
    return -1;
  }

  function clearPlaylist() {
    playlist = [];
    that.notifyAll("playlistCleared");
  }

  function addToPlaylist(track) {
    if (getTrackPositionFromPlaylist(track) === -1) {
      playlist.push(track);
      that.notifyAll("trackAdded", track);
    }
  }

  function selectTrack(track) {
    if (!track) {
      return;
    }
    stopCurrentTrack();
    currentTrack = track;
  }

  function selectNextTrack() {
    var nextTrack, currentTrackPosition = getTrackPositionFromPlaylist(
      currentTrack);
    if (currentTrackPosition < playlist.length - 1) {
      nextTrack = playlist[currentTrackPosition + 1];
    } else {
      nextTrack = playlist[0];
    }
    selectTrack(nextTrack);
    playCurrentTrack();
  }

  function playCurrentTrack() {
    sc.createStream(currentTrack.id, onStreamReady);
  }

  function stopCurrentTrack() {
    if (!currentPlayer) {
      return;
    }
    currentPlayer.pause();
    currentPlayer.seek(TRACK_START_POSITION);
  }

  function pauseCurrentTrack() {
    if (!currentPlayer) {
      return;
    }
    currentPlayer.pause();
  }

  function continueCurrentTrack() {
    if (!currentPlayer) {
      return;
    }
    currentPlayer.play();
  }

  that.addTrackToPlaylist = addToPlaylist;
  that.clearPlaylist = clearPlaylist;
  that.selectTrack = selectTrack;
  that.play = playCurrentTrack;
  that.pause = pauseCurrentTrack;
  that.continue = continueCurrentTrack;
  return that;
};
