var MmePlayer = MmePlayer || {};
MmePlayer.PlayList = function(options) {
  "use strict";
  /* global _, MMEUtils */

  var that = new MMEUtils.EventPublisher(),
    listNode = options.target,
    tracks = [],
    createEntryFromTemplate = _.template(options.entryTemplate);

  function getTrackFromMouseEvent(event) {
    var parent, trackPosition;
    if (event.target.classList.contains("entry")) {
      parent = event.target;
    } else {
      parent = MMEUtils.findParentWithClass(event.target, "entry");
    }
    trackPosition = parseInt(parent.getAttribute("position"));
    return tracks[trackPosition];
  }

  function onListEntrySelected(event) {
    var track = getTrackFromMouseEvent(event);
    if (track) {
      that.notifyAll("trackSelected", track);
    }
  }

  function onPausedButtonPressed(event) {
    var track = getTrackFromMouseEvent(event);
    event.stopPropagation();
    toggleTrackControlls(track);
    if (track) {
      that.notifyAll("trackPaused", track);
    }
  }

  function onPlayButtonPressed(event) {
    var track = getTrackFromMouseEvent(event);
    event.stopPropagation();
    toggleTrackControlls(track);
    if (track) {
      that.notifyAll("trackContinued", track);
    }
  }

  function toggleTrackControlls(track) {
    var node = listNode.querySelector("[streamid='" + track.id + "']"),
      pauseButton = node.querySelector(".button.pause"),
      playButton = node.querySelector(".button.play");
    pauseButton.classList.toggle("hidden");
    playButton.classList.toggle("hidden");
  }

  function addTrack(track) {
    var entry = document.createElement("div");
    track.position = tracks.length;
    tracks.push(track);
    entry.innerHTML = createEntryFromTemplate(track);
    entry.children[0].addEventListener("click", onListEntrySelected);
    entry.querySelector(".button.pause").addEventListener("click",
      onPausedButtonPressed);
    entry.querySelector(".button.play").addEventListener("click",
      onPlayButtonPressed);
    listNode.appendChild(entry.children[0]);
  }

  function setActiveTrack(track) {
    var node = listNode.querySelector("[streamid='" + track.id + "']");
    MMEUtils.removeClassFromNodes(listNode.querySelectorAll(".entry"),
      "selected");
    MMEUtils.addClassToNodes(listNode.querySelectorAll(".button"), "hidden");
    node.querySelector(".button.pause").classList.remove("hidden");
    node.classList.add("selected");
  }

  function clearList() {
    MMEUtils.clearNode(listNode);
  }

  that.addTrack = addTrack;
  that.setActiveTrack = setActiveTrack;
  that.clear = clearList;
  return that;
};
