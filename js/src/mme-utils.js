window.MMEUtils = window.MMEUtils || (function() {
  "use strict";
  var that = {},
    EventPublisher;

  EventPublisher = function() {
    this.listeners = {};
  };

  EventPublisher.prototype.addEventListener = function(event,
    listener) {
    if (this.listeners[event] === undefined) {
      this.listeners[event] = [];
    }
    this.listeners[event].push(listener);
  };

  EventPublisher.prototype.removeEventListener = function(event,
    listener) {
    var index;
    if (this.listeners[event] === undefined) {
      return;
    }
    index = this.listeners[event].indexOf(listener);
    if (index > -1) {
      this.listeners[event].splice(index, 1);
    }
  };

  EventPublisher.prototype.notifyAll = function(event, data) {
    var i;
    for (i = 0; i < this.listeners[event].length; i++) {
      this.listeners[event][i]({
        target: this,
        data: data,
      });
    }
  };

  function findParentWithClass(child, targetClass) {
    var parent = child.parentNode;
    while (!parent.classList.contains(targetClass)) {
      parent = parent.parentNode;
      if (parent === undefined) {
        break;
      }
    }
    return parent;
  }

  function millisecondsToReadableString(ms) {
    var minutes = Math.floor((ms / 1000) / 60),
      seconds = Math.floor((ms / 1000) % 60);
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  }

  function clearNode(node) {
    while (node.hasChildNodes()) {
      node.removeChild(node.lastChild);
    }
  }

  function hideElement(element) {
    element.classList.add("hidden");
  }

  function showElement(element) {
    element.classList.remove("hidden");
  }

  function removeClassFromNodes(nodes, cssClass) {
    var i;
    for (i = 0; i < nodes.length; i++) {
      nodes[i].classList.remove(cssClass);
    }
  }

  function addClassToNodes(nodes, cssClass) {
    var i;
    for (i = 0; i < nodes.length; i++) {
      nodes[i].classList.add(cssClass);
    }
  }

  that.findParentWithClass = findParentWithClass;
  that.millisecondsToReadableString = millisecondsToReadableString;
  that.clearNode = clearNode;
  that.hideElement = hideElement;
  that.showElement = showElement;
  that.removeClassFromNodes = removeClassFromNodes;
  that.addClassToNodes = addClassToNodes;
  that.EventPublisher = EventPublisher;
  return that;
}());
