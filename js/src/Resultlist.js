var MmePlayer = MmePlayer || {};
MmePlayer.ResultList = function(options) {
  "use strict";
  /* global _, MMEUtils */

  var that = new MMEUtils.EventPublisher(),
    listNode = options.target,
    createEntryFromTemplate = _.template(options.entryTemplate),
    currentResults;

  function onListEntrySelected(event) {
    var parent = MMEUtils.findParentWithClass(event.target, "entry"),
      trackPosition = parseInt(parent.getAttribute("position")),
      track = currentResults[trackPosition];
    if (track) {
      that.notifyAll("trackSelected", track);
    }
  }

  function addTrackEntry(track) {
    var entry = document.createElement("div");
    entry.innerHTML = createEntryFromTemplate(track);
    entry.querySelector(".add-to-playlist-button").addEventListener("click",
      onListEntrySelected);
    listNode.appendChild(entry.children[0]);
  }

  function renderSearchResults(results) {
    var i, track;
    currentResults = results;
    for (i = 0; i < currentResults.length; i++) {
      track = currentResults[i]; 
      track["readable_duration"] = MMEUtils.millisecondsToReadableString(track.duration);
      track.position = i;
      addTrackEntry(track);
    }
    MMEUtils.showElement(listNode);
  }

  function clearList() {
    MMEUtils.clearNode(listNode);
    currentResults = undefined;
  }

  that.renderSearchResults = renderSearchResults;
  that.clear = clearList;
  return that;
};
