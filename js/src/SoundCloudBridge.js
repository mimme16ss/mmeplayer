var MmePlayer = MmePlayer || {};
MmePlayer.SoundCloudBridge = function() {
  "use strict";
  /* global SC, MMEUtils */

  var that = new MMEUtils.EventPublisher(),
    SEARCH_RESULT_CUTOFF = 100;

  function init(options) {
    SC.initialize({
      "client_id": options.ID,
      "redirect_uri": "",
    });
  }

  function getStreamableTracksOnly(list) {
    var i,tmp = list;
    for (i = 0; i < tmp.length; i++) {
      if (!tmp[i].streamable) {
        tmp.splice(i, 1);
      }
    }
    return tmp;
  }

  function search(type, query, options) {
    var streamableTracks, queryOptions = options || {};
    queryOptions.q = query;
    queryOptions.limit = SEARCH_RESULT_CUTOFF;
    queryOptions.filter = "streamable";
    SC.get(type, queryOptions).then(function(result) {
      streamableTracks = getStreamableTracksOnly(result);
      that.notifyAll("tracksFound", streamableTracks);
    });
  }

  function createStream(id, callback) {
    SC.stream("/tracks/" + id).then(function(player) {
      if (callback) {
        callback(player);
      } else {
        that.notifyAll("streamReady", player);
      }
    });
  }

  that.init = init;
  that.createStream = createStream;
  that.searchSongs = search.bind(this, "/tracks");
  return that;
};
