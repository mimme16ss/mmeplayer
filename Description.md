# BirdingApp

Diese Dokumentation beschreibt eine exemplarische Lösung für die [zweite Übungsaufgabe](http://mme-player.regensburger-forscher.de) des MME-Kurses im Sommersemester 2016. Den Quellcode sowie eine Anleitung zur Verwendung finden Sie in [diesem Git-Repository](https://bitbucket.org/mimme16ss/mmeplayer). Die einzelnen Module sind hier dokumentiert und beschrieben:

* [MmePlayer](MmePlayer.html)
* [SoundCloudBridge](MmePlayer.SoundCloudBridge.html)
* [SoundCloudPlayer](MmePlayer.SoundCloudPlayer.html)
* [SearchController](MmePlayer.SearchController.html)
* [Playlist](MmePlayer.Playlist.html)
* [Resultlist](MmePlayer.Resultlist.html)
* [MMEUtils](MMEUtils.html)

Alexander Bazo <alexander.bazo@ur.de>