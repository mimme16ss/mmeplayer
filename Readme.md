# MmePlayer

## Abhängigkeiten (Dependencies)

* *Custom Icon Font: Fontello* [http://fontello.com/]
* *underscore.js* [http://underscorejs.org/]
* *Node.js* [https://nodejs.org/en/]
* *Rokkit Font* [https://www.google.com/fonts/specimen/Rokkitt]

## Build

1. Erstellen Sie einen Ordner `dependencies` im Projektverzeichnis
2. Kopieren Sie den Inhalt des [Dependencies-Paket](http://cloud.regensburger-forscher.de:8000/f/ff587383e7/?dl=1) nach `dependencies`.
4. Führen Sie `npm install` im Projektverzeichnis aus

Anschließend kann die Live-Version mit `grunt`, und die Demo-Version mit `grunt demo` gebaut werden. Die fertigen Ergebnisse finden sich im `build`-Ordner. Eine Dokumentation der Anwendung und ihrer Module kann über den Befehl `grunt docs` erstellt werden. 

