module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    eslint: {
      src: ["js/src/*.js"],
    },
    clean: {
      build: "build/",
      docs: "docs/",
      tmp: ["build/res/css/mme-player.css"],
    },
    mkdir: {
      build: {
        options: {
          create: ["build", "build/js/libs", "build/res/css",
            "build/res/img"
          ],
        },
      },
    },
    concat: {
      build: {
        src: ["res/css/styles.css", "res/css/app.css"],
        dest: "build/res/css/mme-player.css",
      },
    },
    cssmin: {
      build: {
        files: [{
          expand: true,
          cwd: "build/res/css",
          src: ["*.css"],
          dest: "build/res/css",
          ext: ".min.css",
        }],
      },
    },
    uglify: {
      options: {
        preserveComments: false,
      },
      build: {
        files: {
          'build/js/app.min.js': ["js/src/MmePlayer.js",
            "js/src/SoundCloudBridge.js", "js/src/SoundCloudPlayer.js",
            "js/src/SearchController.js", "js/src/Resultlist.js",
            "js/src/Playlist.js"
          ],
        },
      },
    },
    copy: {
      build: {
        files: [{
          expand: true,
          src: ["index.html"],
          dest: "build/",
          filter: "isFile",
        }, {
          expand: true,
          src: ["LICENSE"],
          dest: "build/",
          filter: "isFile",
        }, {
          expand: true,
          cwd: "res/icons",
          src: ["*.png"],
          dest: "build/res/img",
          filter: "isFile",
        }, {
          expand: true,
          cwd: "js/src",
          src: ["mme-utils.js"],
          dest: "build/js/libs/",
          filter: "isFile",
        }, {
          expand: true,
          cwd: "dependencies",
          src: ["underscore-min.js"],
          dest: "build/js/libs/",
          filter: "isFile",
        }, {
          expand: true,
          cwd: "dependencies",
          src: ["fontello/*/*"],
          dest: "build/res/",
        }, {
          expand: true,
          cwd: "dependencies",
          src: ["fonts/*/*"],
          dest: "build/res/",
        }, ],
      },
      demo: {
        files: [{
          expand: true,
          src: ["index_demo.html"],
          dest: "build/",
          filter: "isFile",
          rename: function(dest, src) {
            return dest + "index.html";
          },
        }, {
          expand: true,
          cwd: "js/src/",
          src: ["*.js"],
          dest: "build/js/",
        }, ],
      },
    },
    jsdoc: {
      docs: {
        src: ["Description.md", "js/src/**/*.js",
          "dependencies/mme-utils.js"
        ],
        options: {
          destination: "docs",
          access: "all",
        },
      },
    },
  });

  grunt.loadNpmTasks("gruntify-eslint");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-mkdir");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-jsdoc");
  grunt.registerTask("default", ["eslint", "clean:build", "mkdir:build", "concat:build", "cssmin:build", "uglify:build", "copy:build", "clean:tmp"]);
  grunt.registerTask("demo", ["eslint", "clean:build", "mkdir:build", "concat:build", "cssmin:build", "copy:build", "copy:demo", "clean:tmp"]);
  grunt.registerTask("docs", ["clean:docs", "jsdoc:docs"]);

};
